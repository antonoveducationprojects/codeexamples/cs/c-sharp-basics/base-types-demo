﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 08.02.2018
 * Time: 12:43
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;

namespace BaseTypeTesting
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Using DataTypes Example!");
			Console.WriteLine("Byte  Min={0}\t\tMax={1}",byte.MinValue,byte.MaxValue);
			Console.WriteLine("SByte Min={0}\t\tMax={1}",sbyte.MinValue,sbyte.MaxValue);
			Console.WriteLine("Int   Min={0}\t\tMax={1}",int.MinValue,int.MaxValue);
			Console.WriteLine("Int   Min={0}\t\tMax={1}",uint.MinValue,uint.MaxValue);
			Console.WriteLine("Long    Min={0}\t\tMax={1}",long.MinValue,long.MaxValue);
			Console.WriteLine("ULong   Min={0}\t\tMax={1}",ulong.MinValue,ulong.MaxValue);
			Console.WriteLine("Int   Min={0}\t\tMax={1}");

			Console.WriteLine("Single Min={0}\t\tMax={1}", Single.MinValue,Single.MaxValue);
			Console.WriteLine("Single  Min={0}\t\tMax={1}",Single.NegativeInfinity, Single.PositiveInfinity);
			Console.WriteLine("Doudle Min={0}\t\tMax={1}",sbyte.MinValue,sbyte.MaxValue);
			Console.WriteLine("Int   Min={0}\t\tMax={1}",int.MinValue,int.MaxValue);
			Console.WriteLine("Int   Min={0}\t\tMax={1}",uint.MinValue,uint.MaxValue);
			Console.WriteLine("Long    Min={0}\t\tMax={1}",long.MinValue,long.MaxValue);
			Console.WriteLine("ULong   Min={0}\t\tMax={1}",ulong.MinValue,ulong.MaxValue);
			Console.WriteLine("Int   Min={0}\t\tMax={1}");			


			Console.WriteLine("\n\n");			
			
			Console.WriteLine("C:\\Users\\yuriy\\");
			Console.WriteLine(@"C:\Users\yuriy\");			
			
			
string testStr = @"Це новий
                     дуже дуже
                       страний рядок!!!";
			Console.WriteLine(testStr);
			
			
			Console.WriteLine("\n\n");			

			StringBuilder str1 = new StringBuilder("Just for fun!!!",500),
			              str2 = new StringBuilder("Dot Net");
			Console.WriteLine(str1);
			str2.Append(" is not ");
			str2.AppendLine("crossplatform");
			str2.AppendLine("But Dot Net Core is Crossplatform");
			Console.WriteLine(str2);			
			Console.WriteLine("\n\n");					
			
			int x=2000,y=1200;
			sbyte z=unchecked((sbyte)(x-y));
			Console.WriteLine("Z={0}",z);
			


//			z=unchecked(Convert.ToSByte(x-y));
//			Console.WriteLine("Z={0}",z);
			

            Console.WriteLine("\n\n");				
			string[] strArr=new string[5];
			strArr[0]="123";
			strArr[1]="123.567";
			strArr[2]="10E+4";
			strArr[3]="true";
			strArr[4]="1Eghf4h";

//			Console.WriteLine("string   int    double    bool");
//			foreach(string sparam in strArr)
//				Console.WriteLine("{0}   {1}   {2}     ",sparam,int.Parse(sparam),double.Parse(sparam),bool.Parse(sparam));
			
			Console.WriteLine("String\t\tInt\tDouble\tBool");
			foreach(string sparam in strArr)
			{
				int i=0;
				int.TryParse(sparam,out i);
				double d=0.0;
				double.TryParse(sparam,out d);
				bool b;
				bool.TryParse(sparam, out b);
				Console.WriteLine("{0}\t\t{1}\t{2}\t{3}",sparam,i,d,b);
			}
			
			
			Console.WriteLine("\n\n");								
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);			
			
		}
	}
}